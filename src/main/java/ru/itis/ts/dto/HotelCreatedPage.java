package ru.itis.ts.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "This oages shows when the user creates a new hotel for a city")
public class HotelCreatedPage {

  @Schema(description = "message shown")
  private String message;

  @Schema(description = "The Hotel object that has just been created")
  private HotelDto hotelDto;

}
