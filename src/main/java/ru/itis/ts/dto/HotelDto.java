package ru.itis.ts.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ts.models.City;
import ru.itis.ts.models.Hotel;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "Hotel", description = "Hotel")
public class HotelDto {

  private Long id;

  @JsonProperty("name")
  private String name;

  public static HotelDto from(Hotel hotel){
    return HotelDto
            .builder()
            .id(hotel.getId())
            .name(hotel.getName())
            .build();
  }
}
