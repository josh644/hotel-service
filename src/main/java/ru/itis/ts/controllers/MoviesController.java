package ru.itis.ts.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.ts.controllers.api.MovieApi;
import ru.itis.ts.dto.MovieCreatedPage;
import ru.itis.ts.dto.MovieDto;
import ru.itis.ts.dto.MoviePage;
import ru.itis.ts.services.impl.MovieService;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class MoviesController implements MovieApi {

  @Autowired
  private MovieService movieService;

  @Override
  public ResponseEntity<MovieCreatedPage> createMovie(Long cinemaId, MovieDto movieDto) {
    return ResponseEntity.ok(movieService.crateMovie(cinemaId, movieDto));
  }

  @Override
  public ResponseEntity<MoviePage> getMovies(Long cinemaId) {
    return ResponseEntity.ok(movieService.getMoviesFromCinema(cinemaId));
  }
}
