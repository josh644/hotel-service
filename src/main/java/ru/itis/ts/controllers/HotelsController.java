package ru.itis.ts.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.ts.controllers.api.HotelsApi;
import ru.itis.ts.dto.HotelCreatedPage;
import ru.itis.ts.dto.HotelDto;
import ru.itis.ts.dto.HotelPage;
import ru.itis.ts.services.impl.HotelService;


@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class HotelsController implements HotelsApi {

    @Autowired
    private HotelService hotelService;

    @Override
    public ResponseEntity<HotelPage> getHotels(Long cityId) {
        return ResponseEntity.ok(hotelService.getHotelsByCity(cityId));
    }

    @Override
    public ResponseEntity<HotelCreatedPage> createHotel(Long cityId, HotelDto hotelDto) {
        return ResponseEntity.ok(hotelService.createHotelForCity(cityId, hotelDto));
    }
}
