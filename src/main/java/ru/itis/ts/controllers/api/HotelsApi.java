package ru.itis.ts.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ts.dto.HotelCreatedPage;
import ru.itis.ts.dto.HotelDto;
import ru.itis.ts.dto.HotelPage;

@RequestMapping(path = "/api/cities/{city_id}/hotels")
@Tags(value =
@Tag(name = "Hotels"))
public interface HotelsApi {
    @Operation(summary = "We get a list of hotels", description = "Available for all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = HotelPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<HotelPage> getHotels(@PathVariable("city_id") Long cityId);

    @PostMapping
    ResponseEntity<HotelCreatedPage> createHotel(@PathVariable("city_id") Long cityId, @RequestBody HotelDto hotelDto);
}
