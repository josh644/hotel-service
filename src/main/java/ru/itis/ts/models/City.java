package ru.itis.ts.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class City {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String name;

  private String location;

  @OneToMany(mappedBy = "city")
  private List<Hotel> hotels;

  @OneToMany(mappedBy = "city")
  private List<Cinema> cinemas;
}
