package ru.itis.ts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ts.models.Cinema;

public interface CinemaRepository extends JpaRepository<Cinema, Long> {
}
