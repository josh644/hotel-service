package ru.itis.ts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ts.models.Hotel;

public interface HotelRepository extends JpaRepository<Hotel, Long> {
}
