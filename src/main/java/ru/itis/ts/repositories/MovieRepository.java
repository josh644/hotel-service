package ru.itis.ts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.ts.models.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}
