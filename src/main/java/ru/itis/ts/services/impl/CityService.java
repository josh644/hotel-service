package ru.itis.ts.services.impl;

import ru.itis.ts.dto.CityCreatedPage;
import ru.itis.ts.dto.CityDto;
import ru.itis.ts.dto.CityPage;

public interface CityService {
  CityPage getCities();
  CityCreatedPage createCity(CityDto cityDto);
}
