package ru.itis.ts.services.impl;

import ru.itis.ts.dto.MovieCreatedPage;
import ru.itis.ts.dto.MovieDto;
import ru.itis.ts.dto.MoviePage;

public interface MovieService {
  MovieCreatedPage crateMovie(Long cinemaId, MovieDto movieDto);
  MoviePage getMoviesFromCinema(Long cinemaID);
}

