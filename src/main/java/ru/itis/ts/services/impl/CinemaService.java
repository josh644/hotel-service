package ru.itis.ts.services.impl;

import ru.itis.ts.dto.CinemaPage;

public interface CinemaService {
  CinemaPage getCinemas();
}
