package ru.itis.ts.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.ts.dto.CityDto;
import ru.itis.ts.dto.HotelCreatedPage;
import ru.itis.ts.dto.HotelDto;
import ru.itis.ts.dto.HotelPage;
import ru.itis.ts.models.City;
import ru.itis.ts.models.Hotel;
import ru.itis.ts.repositories.CityRepository;
import ru.itis.ts.repositories.HotelRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HotelsServiceImpl implements HotelService{

  @Autowired
  private HotelRepository hotelRepository;

  @Autowired
  private CityRepository cityRepository;

  public HotelPage getHotels(){
    List<HotelDto> hotels = hotelRepository
            .findAll()
            .stream()
            .map(hotel -> HotelDto.from(hotel))
            .toList();

    return HotelPage
            .builder()
            .hotels(hotels)
            .build();
  }

  public HotelPage getHotelsByCity(Long cityId){
    CityDto cityDto = CityDto.from(cityRepository.findById(cityId).get());

    return HotelPage
            .builder()
            .hotels(cityDto.getHotels())
            .build();
  }

  public HotelCreatedPage createHotelForCity(Long cityId, HotelDto hotelDto){
    City city = cityRepository.findById(cityId).get();

    Hotel hotel = Hotel
            .builder()
            .name(hotelDto.getName())
            .city(city)
            .build();


    Hotel responseHotel;

    if((responseHotel = hotelRepository.save(hotel)) != null){
      return HotelCreatedPage
              .builder()
              .message("The hotel has been created correctly")
              .hotelDto(HotelDto.from(responseHotel))
              .build();
    }
    else{
      return HotelCreatedPage
              .builder()
              .message("Something went wrong")
              .build();
    }
  }
}
