package ru.itis.ts.services.impl;
import ru.itis.ts.dto.HotelCreatedPage;
import ru.itis.ts.dto.HotelDto;
import ru.itis.ts.dto.HotelPage;

public interface HotelService {
  HotelPage getHotels();
  HotelPage getHotelsByCity(Long cityId);
  HotelCreatedPage createHotelForCity(Long cityId, HotelDto hotelDto);
}
